/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from 'vue'
import cookies from 'browser-cookies'
import {
  CHECK,
  LOGIN,
  LOGOUT
} from './mutation-types'

export default {
  [CHECK] (state) {
    state.authenticated = !!cookies.get('AUTH-TOKEN-X')
    if (state.authenticated) {
      Vue.$http.defaults.headers.common.Authorization = `Bearer ${cookies.get('AUTH-TOKEN-X')}`
    }
  },

  [LOGIN] (state, payload) {
    // console.log(payload)
    cookies.set('AUTH-TOKEN-X', payload.token, {expires: 600})
    state.authenticated = true
    Vue.$http.defaults.headers.common.Authorization = `Bearer ${payload.token}`
  },

  [LOGOUT] (state) {
    state.authenticated = false
    cookies.erase('AUTH-TOKEN-X')
    Vue.$http.defaults.headers.common.Authorization = ''
  }
}
