/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import Vue from 'vue'
import store from 'store'
import * as types from './mutation-types'
import Proxy from 'proxies/AuthProxy'
import service from 'services'

export const check = ({ commit }) => {
  commit(types.CHECK)
}

// merge to submit global (form/create)
export const register = ({ commit }, payload) => {
  new Proxy()
     .register(payload)
     .then((response) => {
       try {
         service.errorMessage(response)
       } catch (err) {
         response.status = true
         store.dispatch('message/flashMessage', response)
       }
     })
     .catch(() => {
       console.log('Request failed...')
     })
}

export const login = ({ commit }, payload) => {
  new Proxy()
   .login(payload)
    .then((response) => {
      commit(types.LOGIN, response)
      // store.dispatch('account/find')
      Vue.router.push({
        name: 'home.index'
      })
    })
     .catch(() => {
       console.log('Request failed...')
     })

  Vue.router.push({
    name: 'home.index'
  })
}

export const activate = ({ commit }, payload) => {
  new Proxy().setParameter('token', payload).find('activate')
    .then((response) => {
      commit(types.LOGIN, response)
      store.dispatch('account/find')
      Vue.router.push({
        name: 'home.index'
      })
    })
     .catch(() => {
       store.dispatch('message/flashMessage', {status: true, field: 'Error !', message: 'the token provided is not valid or has expired'})
     })

  /* Vue.router.push({
    name: 'home.index'
  }) */
}

export const recover = ({ commit }, payload) => {
  new Proxy().setParameter('token', payload).find('recover')
    .then((response) => {
      if (response.field !== 'Success') {
        response.status = true
        store.dispatch('message/flashMessage', response)
      }
      // commit(types.LOGIN, response)
      // store.dispatch('account/find')
      /* Vue.router.push({
        name: 'home.index'
      }) */
    })
     .catch(() => {

     })

  /* Vue.router.push({
    name: 'home.index'
  }) */
}

export const logout = ({ commit }) => {
  commit(types.LOGOUT)
  Vue.router.push({
    name: 'login.index'
  })
}

export default {
  check,
  register,
  login,
  activate,
  recover,
  logout
}
