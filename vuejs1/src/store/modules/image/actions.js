/* ============
 * Actions for the error module
 * ============
 *
 * The actions that are available on the
 * error module.
 */

import * as types from './mutation-types'

export const display = ({ commit }, payload) => {
  commit(types.DISPLAY, payload)
}

export default {
  display
}
