/* ============
 * State of the account module
 * ============
 *
 * The initial state of the account module.
 */

export default {
  credential: null,
  firstName: null,
  lastName: null,
  gender: null,
  birthDate: null,
  bestWayToReachYou: null,
  homePhone: null,
  officePhone: null,
  emailContact: null,
  role: []
}
