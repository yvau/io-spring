/* ============
 * Mutations for the account module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from 'vue'
import { FIND, RELOGIN } from './mutation-types'
import cookies from 'browser-cookies'

export default {
  [FIND] (state, account) {
    state.credential = account.credential
    state.firstName = account.firstName
    state.lastName = account.lastName
    state.gender = account.gender
    state.birthDate = account.birthDate
    state.role = account.role
    state.bestWayToReachYou = account.bestWayToReachYou
    state.emailContact = account.emailContact
    state.officePhone = account.officePhone
    state.homePhone = account.homePhone
  },

  [RELOGIN] (state, payload) {
    cookies.set('AUTH-TOKEN-X', payload.token, {expires: 600})
    state.authenticated = true
    Vue.$http.defaults.headers.common.Authorization = `Bearer ${payload.token}`
  }
}
