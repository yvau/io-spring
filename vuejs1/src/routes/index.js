/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [

  // Home
  {
    path: '/',
    name: 'home.index',
    component: () => import('pages/Index/Index'),

    // Any user can see this page
    meta: {
      guest: true
    }
  },

  // About us
  {
    path: '/about-us/',
    name: 'aboutUs.index',
    component: () => import('pages/About/Index'),

    // Any user can see this page
    meta: {
      guest: true
    }
  },

  // About us
  {
    path: '/vone-concept/',
    name: 'voneConcept.index',
    component: () => import('pages/Concept/Index'),

    // Any user can see this page
    meta: {
      guest: true
    }
  },

  // Advantage
  {
    path: '/vone-advantage/',
    name: 'advantage.index',
    component: () => import('pages/Advantage/Index'),

    // Any user can see this page
    meta: {
      guest: true
    }
  },

  // Relationship
  {
    path: '/vone-relationship/',
    name: 'relationship.index',
    component: () => import('pages/Relationship/Index'),

    // Any user can see this page
    meta: {
      guest: true
    }
  },

  // Home
  {
    path: '/social-media-and-blog/',
    name: 'social.index',
    component: () => import('pages/SocialAndBlog/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true
    }
  },

  // Home
  {
    path: '/term-of-use/',
    name: 'termOfUse.index',
    component: () => import('pages/TermOfUse/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true
    }
  },

  // Home
  {
    path: '/feeds/',
    name: 'feeds.index',
    component: () => import('pages/Feeds/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true
    }
  },

  // Home
  {
    path: '/bookmarks/',
    name: 'bookmarks.index',
    component: () => import('pages/Bookmarks/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true
    }
  },

  // Home
  {
    path: '/notifications/',
    name: 'notification.index',
    component: () => import('pages/Feeds/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true
    }
  },

  // Home
  {
    path: '/analytics/',
    name: 'analytics.index',
    component: () => import('pages/Home/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true
    }
  },

  // Account
  {
    path: '/account',
    name: 'account.index',
    component: () => import('pages/Account/Index'),

    // If the user needs to be authenticated to view this page.
    meta: {
      guest: true
    }
  },

  // Login
  {
    path: '/login',
    name: 'login.index',
    component: () => import('pages/Login/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: () => import('pages/Register/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Recover form
  {
    path: '/recover',
    name: 'recoverForm.index',
    component: () => import('pages/Recover/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Recover password
  {
    path: '/recover/password',
    name: 'recoverPassword.index',
    component: () => import('pages/Recover/Password/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Activate account
  {
    path: '/activate',
    name: 'activate.index',
    component: () => import('pages/Activate/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/buy/',
    name: 'buyOverview.index',
    component: () => import('pages/Buy/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile show buy
  {
    path: '/buy/:id(\\d+)',
    name: 'buyShow.index',
    component: () => import('pages/Buy/Show/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/buy/list',
    name: 'buyList.index',
    component: () => import('pages/Buy/List/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/profile/buy/new',
    name: 'profileBuyNew.index',
    component: () => import('pages/Profile/Buy/New/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile show buy
  {
    path: '/profile/buy/:id(\\d+)',
    name: 'profileBuyShow.index',
    component: () => import('pages/Profile/Buy/Show/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit buy
  {
    path: '/profile/buy/edit/:id(\\d+)',
    name: 'profileBuyEdit.index',
    component: () => import('pages/Profile/Buy/Edit/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile list buy
  {
    path: '/profile/buy/list',
    name: 'profileBuyList.index',
    component: () => import('pages/Profile/Buy/List/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/rent/',
    name: 'rentOverview.index',
    component: () => import('pages/Rent/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/rent/list',
    name: 'rentList.index',
    component: () => import('pages/Rent/List/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/profile/rent/new',
    name: 'profileRentNew.index',
    component: () => import('pages/Profile/Rent/New/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile show rent
  {
    path: '/profile/rent/:id(\\d+)',
    name: 'profileRentShow.index',
    component: () => import('pages/Profile/Rent/Show/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit rent
  {
    path: '/profile/rent/edit/:id(\\d+)',
    name: 'profileRentEdit.index',
    component: () => import('pages/Profile/Rent/Edit/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile list rent
  {
    path: '/profile/rent/list',
    name: 'profileRentList.index',
    component: () => import('pages/Profile/Rent/List/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/profile/property/list',
    name: 'profilePropertyList.index',
    component: () => import('pages/Profile/Property/List/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new rent
  {
    path: '/property/list',
    name: 'propertyList.index',
    component: () => import('pages/Property/List/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new property
  {
    path: '/profile/property/new',
    name: 'profilePropertyNew.index',
    component: () => import('pages/Profile/Property/New/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new property
  {
    path: '/profile/property/edit/:id(\\d+)',
    name: 'profilePropertyEdit.index',
    component: () => import('pages/Profile/Property/Edit/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new property
  {
    path: '/lease/',
    name: 'leaseOverview.index',
    component: () => import('pages/Lease/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new property
  {
    path: '/profile/lease/new',
    name: 'profileLeaseNew.index',
    component: () => import('pages/Profile/Lease/New/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile new property
  {
    path: '/sell/',
    name: 'sellOverview.index',
    component: () => import('pages/Sell/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit password
  {
    path: '/profile/edit/password',
    name: 'profileEditPassword.index',
    component: () => import('pages/Profile/Edit/Password/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit role
  {
    path: '/profile/edit/role',
    name: 'profileEditRole.index',
    component: () => import('pages/Profile/Edit/Role/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit contacts
  {
    path: '/profile/edit/contact',
    name: 'profileEditContact.index',
    component: () => import('pages/Profile/Edit/Contact/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit information
  {
    path: '/profile/edit/information',
    name: 'profileEditInformation.index',
    component: () => import('pages/Profile/Edit/Information/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit information
  {
    path: '/profile/payment/paypal',
    name: 'profilePaymentPaypal.index',
    component: () => import('pages/Profile/Payment/Paypal/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Profile edit information
  {
    path: '/profile/payment/card',
    name: 'profilePaymentCard.index',
    component: () => import('pages/Profile/Payment/Card/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  {
    path: '/',
    redirect: '/home'
  },

  {
    path: '/*',
    redirect: '/'
  }
]
