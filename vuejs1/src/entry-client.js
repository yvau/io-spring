import { createApp } from 'main'
import 'plugins/font-awesome'
import 'plugins/pace'
import 'plugins/multiselect'
import 'plugins/validator'
import 'plugins/stylesheet'
// client-specific bootstrapping logic...

const { app } = createApp()

// this assumes App.vue template root element has `id="app"`
app.$mount('#app')
