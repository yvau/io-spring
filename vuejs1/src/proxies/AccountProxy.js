import Proxy from './Proxy'

class AccountProxy extends Proxy {
  /**
   * The constructor for the ArtistProxy.
   *
   * @param {Object} parameters The query parameters.
   */
  constructor (parameters = {}) {
    super('api/profile', parameters)
  }

  /**
   * Method used to login.
   *
   * @param {String} username The username.
   * @param {String} password The password.
   *
   * @returns {Promise} The result in a promise.
   */
  changeRole (data) {
    return this.submit('put', `${this.endpoint}/edit/role`, data)
  }
}

export default AccountProxy
