/* eslint-disable no-undef */
import awaitServer from './awaitServer'
// import router from './router/routerServer'
import App from './App'
global.renderApp = function (_viewName, model, url) {
  let assetManifest = BUILD_MANIVEST
  let cssPath = 'static/css/main.css'
  let manifest = 'static/js/manifest.js'
  let vendor = 'static/js/vendor.js'
  let app = 'static/js/app.js'

  if (assetManifest) {
    manifest = assetManifest['manifest.js']
    vendor = assetManifest['vendor.js']
    app = assetManifest['app.js']
    cssPath = assetManifest['app.css']
  }

  const requestPath = getData(model)
  console.log(requestPath)

  function populateTemplate (markup) {
    return `<!doctype html>
    <html>
     <head>
       <link rel='stylesheet' href='/${cssPath}' />
      </head>
  <body class='fixed-header horizontal-menu horizontal-app-menu dashboard'>
    ${markup}
    <script type='text/javascript' src='https://code.jquery.com/jquery-2.1.4.min.js'></script>
    <script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'
     integrity='sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS' crossorigin='anonymous'></script>
   <script type='text/javascript' src='/${manifest}'></script>
   <script type='text/javascript' src='/${vendor}'></script>
   <script type='text/javascript' src='/${app}'></script>
 </body>
</html>`
  }

  // const serializer = (new Packages.com.fasterxml.jackson.databind.ObjectMapper()).writer()

  function getData (model) {
    const renderData = { data: {} }
    for (let key in model) {
      if (key.startsWith('__')) {
        renderData[ key.substring(2) ] = model[key]
      } else {
        renderData.data[key] = model[key]
      }
    }

    /* Serialise the model for passing to the client. We don't use JSON.stringify
     * because Nashorn's version doesn't cope with POJOs by design.
     *
     * http://www.slideshare.net/SpringCentral/serverside-javascript-with-nashorn-and-spring
     */
    // renderData.json = serializer.writeValueAsString(renderData.data)

    /* "Purify" the model by swapping it for the serialised version */
    // renderData.data = JSON.parse(renderData.json)

    return renderData
  }

  let vm = new Vue({
    template: '<App/>',
    components: { App }
  })

  let inWait = awaitServer(function (done) {
    renderVueComponentToString(vm, function (err, res) {
      done(err, res)
    })
  })

  if (inWait.error) {
    console.log(inWait.error)
  } else {
    return populateTemplate(inWait.result)
  }
}
