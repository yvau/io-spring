/* ============
 * Account Transformer
 * ============
 *
 * The transformer for the account.
 */

import Transformer from './Transformer'

export default class AccountTransformer extends Transformer {
  /**
   * Method used to transform a fetched account.
   *
   * @param account The fetched account.
   *
   * @returns {Object} The transformed account.
   */
  static fetch (account) {
    return {
      credential: account.credential,
      firstName: account.profileInformation.firstName,
      lastName: account.profileInformation.lastName,
      gender: account.profileInformation.gender,
      birthDate: account.profileInformation.birthDate,
      bestWayToReachYou: account.profileInformation.bestWayToReachYou,
      homePhone: account.profileInformation.homePhone,
      officePhone: account.profileInformation.officePhone,
      emailContact: account.profileInformation.emailContact,
      role: account.role.split(',')
    }
  }

  /**
   * Method used to transform a send account.
   *
   * @param account The account to be send.
   *
   * @returns {Object} The transformed account.
   */
  static send (account) {
    return {
      email: account.email,
      first_name: account.firstName,
      last_name: account.lastName
    }
  }
}
