package io.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property database table.
 * 
 */
@Entity
@NamedQuery(name="Property.findAll", query="SELECT p FROM Property p")
public class Property implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private Integer bathrooms;

	private Integer bedrooms;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String characteristics;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String description;

	private Boolean enabled;

	private BigDecimal price;

	@Column(name="sale_type")
	private String saleType;

	private BigDecimal size;

	private String status;

	private String type;

	//bi-directional many-to-one association to Bookmark
	@OneToMany(mappedBy="property")
	@JsonIgnore
	private List<Bookmark> bookmarks;

	//bi-directional many-to-one association to Location
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonManagedReference
	private Location location;

	//bi-directional many-to-one association to Payment
	@ManyToOne
	@JsonIgnore
	private Payment payment;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	@JsonManagedReference
	private Profile profile;

	//bi-directional many-to-one association to PropertyPhoto
	@OneToMany(mappedBy="property")
	@JsonIgnore
	private List<PropertyPhoto> propertyPhotos;

	//bi-directional many-to-one association to ProposalHasProperty
	@OneToMany(mappedBy="property")
	@JsonIgnore
	private List<ProposalHasProperty> proposalHasProperties;

	//bi-directional many-to-one association to ProfileActivityDetail
	@OneToMany(mappedBy="property")
	@JsonIgnore
	private List<ProfileActivityDetail> profileActivityDetails;

	public Property() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBathrooms() {
		return this.bathrooms;
	}

	public void setBathrooms(Integer bathrooms) {
		this.bathrooms = bathrooms;
	}

	public Integer getBedrooms() {
		return this.bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public String getCharacteristics() {
		return this.characteristics;
	}

	public void setCharacteristics(String characteristics) {
		this.characteristics = characteristics;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSaleType() {
		return this.saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public BigDecimal getSize() {
		return this.size;
	}

	public void setSize(BigDecimal size) {
		this.size = size;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Bookmark> getBookmarks() {
		return this.bookmarks;
	}

	public void setBookmarks(List<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}

	public Bookmark addBookmark(Bookmark bookmark) {
		getBookmarks().add(bookmark);
		bookmark.setProperty(this);

		return bookmark;
	}

	public Bookmark removeBookmark(Bookmark bookmark) {
		getBookmarks().remove(bookmark);
		bookmark.setProperty(null);

		return bookmark;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Payment getPayment() {
		return this.payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<PropertyPhoto> getPropertyPhotos() {
		return this.propertyPhotos;
	}

	public void setPropertyPhotos(List<PropertyPhoto> propertyPhotos) {
		this.propertyPhotos = propertyPhotos;
	}

	public PropertyPhoto addPropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().add(propertyPhoto);
		propertyPhoto.setProperty(this);

		return propertyPhoto;
	}

	public PropertyPhoto removePropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().remove(propertyPhoto);
		propertyPhoto.setProperty(null);

		return propertyPhoto;
	}

	public List<ProposalHasProperty> getProposalHasProperties() {
		return this.proposalHasProperties;
	}

	public void setProposalHasProperties(List<ProposalHasProperty> proposalHasProperties) {
		this.proposalHasProperties = proposalHasProperties;
	}

	public ProposalHasProperty addProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().add(proposalHasProperty);
		proposalHasProperty.setProperty(this);

		return proposalHasProperty;
	}

	public ProposalHasProperty removeProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().remove(proposalHasProperty);
		proposalHasProperty.setProperty(null);

		return proposalHasProperty;
	}

	public List<ProfileActivityDetail> getProfileActivityDetails() {
		return this.profileActivityDetails;
	}

	public void setProfileActivityDetails(List<ProfileActivityDetail> profileActivityDetails) {
		this.profileActivityDetails = profileActivityDetails;
	}

	public ProfileActivityDetail addProfileActivityDetail(ProfileActivityDetail profileActivityDetail) {
		getProfileActivityDetails().add(profileActivityDetail);
		profileActivityDetail.setProperty(this);

		return profileActivityDetail;
	}

	public ProfileActivityDetail removeProfileActivityDetail(ProfileActivityDetail profileActivityDetail) {
		getProfileActivityDetails().remove(profileActivityDetail);
		profileActivityDetail.setProperty(null);

		return profileActivityDetail;
	}

}