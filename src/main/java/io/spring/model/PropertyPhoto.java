package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the property_photo database table.
 * 
 */
@Entity
@Table(name="property_photo")
@NamedQuery(name="PropertyPhoto.findAll", query="SELECT p FROM PropertyPhoto p")
public class PropertyPhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="content_type")
	private String contentType;

	@Column(name="created_date")
	private Timestamp createdDate;

	private String name;

	private BigDecimal size;

	@Column(name="thumbnail_name")
	private String thumbnailName;

	@Column(name="thumbnail_size")
	private BigDecimal thumbnailSize;

	private String type;

	private String url;

	//bi-directional many-to-one association to Property
	@ManyToOne
	private Property property;

	public PropertyPhoto() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSize() {
		return this.size;
	}

	public void setSize(BigDecimal size) {
		this.size = size;
	}

	public String getThumbnailName() {
		return this.thumbnailName;
	}

	public void setThumbnailName(String thumbnailName) {
		this.thumbnailName = thumbnailName;
	}

	public BigDecimal getThumbnailSize() {
		return this.thumbnailSize;
	}

	public void setThumbnailSize(BigDecimal thumbnailSize) {
		this.thumbnailSize = thumbnailSize;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

}