package io.spring.model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the auto_increment database table.
 * 
 */
@Entity
@Table(name="auto_increment")
@NamedQuery(name="AutoIncrement.findAll", query="SELECT a FROM AutoIncrement a")
public class AutoIncrement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Column(name="increment_number")
	private Long incrementNumber;

	public AutoIncrement() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getIncrementNumber() {
		return this.incrementNumber;
	}

	public void setIncrementNumber(Long incrementNumber) {
		this.incrementNumber = incrementNumber;
	}

}