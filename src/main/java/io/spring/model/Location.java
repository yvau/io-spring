package io.spring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String address;

	@Column(name="postal_code")
	private String postalCode;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JsonManagedReference
	private City city;

	//bi-directional many-to-one association to Country
	@ManyToOne
	@JsonManagedReference
	private Country country;

	//bi-directional many-to-one association to Province
	@ManyToOne
	@JsonManagedReference
	private Province province;

	//bi-directional many-to-one association to Property
	@OneToMany(mappedBy="location")
	@JsonBackReference
	private List<Property> properties;

	//bi-directional many-to-many association to Proposal
	@ManyToMany(mappedBy="locations")
	@JsonIgnore
	private List<Proposal> proposals;

	//bi-directional many-to-one association to SearchProposalDetail
	@OneToMany(mappedBy="location")
	@JsonIgnore
	private List<SearchProposalDetail> searchProposalDetails;

	public Location() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public List<Property> getProperties() {
		return this.properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	public Property addProperty(Property property) {
		getProperties().add(property);
		property.setLocation(this);

		return property;
	}

	public Property removeProperty(Property property) {
		getProperties().remove(property);
		property.setLocation(null);

		return property;
	}

	public List<Proposal> getProposals() {
		return this.proposals;
	}

	public void setProposals(List<Proposal> proposals) {
		this.proposals = proposals;
	}

	public List<SearchProposalDetail> getSearchProposalDetails() {
		return this.searchProposalDetails;
	}

	public void setSearchProposalDetails(List<SearchProposalDetail> searchProposalDetails) {
		this.searchProposalDetails = searchProposalDetails;
	}

	public SearchProposalDetail addSearchProposalDetail(SearchProposalDetail searchProposalDetail) {
		getSearchProposalDetails().add(searchProposalDetail);
		searchProposalDetail.setLocation(this);

		return searchProposalDetail;
	}

	public SearchProposalDetail removeSearchProposalDetail(SearchProposalDetail searchProposalDetail) {
		getSearchProposalDetails().remove(searchProposalDetail);
		searchProposalDetail.setLocation(null);

		return searchProposalDetail;
	}

}