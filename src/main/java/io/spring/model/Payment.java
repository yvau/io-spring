package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the payment database table.
 * 
 */
@Entity
@NamedQuery(name="Payment.findAll", query="SELECT p FROM Payment p")
public class Payment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private Integer count;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	@Column(name="id_transaction")
	private String idTransaction;

	private String method;

	private String status;

	//bi-directional many-to-one association to PackageProduct
	@ManyToOne
	@JoinColumn(name="package_product_id")
	private PackageProduct packageProduct;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to Property
	@OneToMany(mappedBy="payment")
	private List<Property> properties;

	//bi-directional many-to-one association to ProposalHasProperty
	@OneToMany(mappedBy="payment")
	private List<ProposalHasProperty> proposalHasProperties;

	public Payment() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCount() {
		return this.count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public String getIdTransaction() {
		return this.idTransaction;
	}

	public void setIdTransaction(String idTransaction) {
		this.idTransaction = idTransaction;
	}

	public String getMethod() {
		return this.method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PackageProduct getPackageProduct() {
		return this.packageProduct;
	}

	public void setPackageProduct(PackageProduct packageProduct) {
		this.packageProduct = packageProduct;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<Property> getProperties() {
		return this.properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	public Property addProperty(Property property) {
		getProperties().add(property);
		property.setPayment(this);

		return property;
	}

	public Property removeProperty(Property property) {
		getProperties().remove(property);
		property.setPayment(null);

		return property;
	}

	public List<ProposalHasProperty> getProposalHasProperties() {
		return this.proposalHasProperties;
	}

	public void setProposalHasProperties(List<ProposalHasProperty> proposalHasProperties) {
		this.proposalHasProperties = proposalHasProperties;
	}

	public ProposalHasProperty addProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().add(proposalHasProperty);
		proposalHasProperty.setPayment(this);

		return proposalHasProperty;
	}

	public ProposalHasProperty removeProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().remove(proposalHasProperty);
		proposalHasProperty.setPayment(null);

		return proposalHasProperty;
	}

}