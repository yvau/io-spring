package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the profile_activity_details database table.
 * 
 */
@Entity
@Table(name="profile_activity_details")
@NamedQuery(name="ProfileActivityDetail.findAll", query="SELECT p FROM ProfileActivityDetail p")
public class ProfileActivityDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	private String type;

	//bi-directional many-to-one association to ProfileActivity
	@ManyToOne
	@JoinColumn(name="profile_activity_id")
	private ProfileActivity profileActivity;

	//bi-directional many-to-one association to Property
	@ManyToOne
	private Property property;

	//bi-directional many-to-one association to Proposal
	@ManyToOne
	private Proposal proposal;

	public ProfileActivityDetail() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ProfileActivity getProfileActivity() {
		return this.profileActivity;
	}

	public void setProfileActivity(ProfileActivity profileActivity) {
		this.profileActivity = profileActivity;
	}

	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Proposal getProposal() {
		return this.proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

}