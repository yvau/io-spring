package io.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the proposal database table.
 * 
 */
@Entity
@NamedQuery(name="Proposal.findAll", query="SELECT p FROM Proposal p")
public class Proposal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="age_of_property")
	private String ageOfProperty;

	private Integer bathrooms;

	private Integer bedrooms;

	private Boolean enabled;

	private String features;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	@Column(name="is_furnished")
	private Boolean isFurnished;

	@Column(name="is_urgent")
	private Boolean isUrgent;

	private String name;

	@Column(name="price_maximum")
	private BigDecimal priceMaximum;

	@Column(name="price_minimum")
	private BigDecimal priceMinimum;

	private BigDecimal size;

	private String status;

	@Column(name="type_of_property")
	private String typeOfProperty;

	@Column(name="type_of_proposal")
	private String typeOfProposal;

	//bi-directional many-to-one association to Bookmark
	@OneToMany(mappedBy="proposal")
	@JsonIgnore
	private List<Bookmark> bookmarks;

	//bi-directional many-to-many association to Location
	@ManyToMany
	@JoinTable(
		name="proposal_has_location"
		, joinColumns={
			@JoinColumn(name="proposal_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="location_id")
			}
		)
	private List<Location> locations;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	@JsonManagedReference
	private Profile profile;

	//bi-directional many-to-one association to ProposalHasProperty
	@OneToMany(mappedBy="proposal")
	@JsonIgnore
	private List<ProposalHasProperty> proposalHasProperties;

	//bi-directional many-to-one association to ProfileActivityDetail
	@OneToMany(mappedBy="proposal")
	@JsonIgnore
	private List<ProfileActivityDetail> profileActivityDetails;

	public Proposal() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgeOfProperty() {
		return this.ageOfProperty;
	}

	public void setAgeOfProperty(String ageOfProperty) {
		this.ageOfProperty = ageOfProperty;
	}

	public Integer getBathrooms() {
		return this.bathrooms;
	}

	public void setBathrooms(Integer bathrooms) {
		this.bathrooms = bathrooms;
	}

	public Integer getBedrooms() {
		return this.bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getFeatures() {
		return this.features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public Timestamp getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Boolean getIsFurnished() {
		return this.isFurnished;
	}

	public void setIsFurnished(Boolean isFurnished) {
		this.isFurnished = isFurnished;
	}

	public Boolean getIsUrgent() {
		return this.isUrgent;
	}

	public void setIsUrgent(Boolean isUrgent) {
		this.isUrgent = isUrgent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPriceMaximum() {
		return this.priceMaximum;
	}

	public void setPriceMaximum(BigDecimal priceMaximum) {
		this.priceMaximum = priceMaximum;
	}

	public BigDecimal getPriceMinimum() {
		return this.priceMinimum;
	}

	public void setPriceMinimum(BigDecimal priceMinimum) {
		this.priceMinimum = priceMinimum;
	}

	public BigDecimal getSize() {
		return this.size;
	}

	public void setSize(BigDecimal size) {
		this.size = size;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTypeOfProperty() {
		return this.typeOfProperty;
	}

	public void setTypeOfProperty(String typeOfProperty) {
		this.typeOfProperty = typeOfProperty;
	}

	public String getTypeOfProposal() {
		return this.typeOfProposal;
	}

	public void setTypeOfProposal(String typeOfProposal) {
		this.typeOfProposal = typeOfProposal;
	}

	public List<Bookmark> getBookmarks() {
		return this.bookmarks;
	}

	public void setBookmarks(List<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}

	public Bookmark addBookmark(Bookmark bookmark) {
		getBookmarks().add(bookmark);
		bookmark.setProposal(this);

		return bookmark;
	}

	public Bookmark removeBookmark(Bookmark bookmark) {
		getBookmarks().remove(bookmark);
		bookmark.setProposal(null);

		return bookmark;
	}

	public List<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<ProposalHasProperty> getProposalHasProperties() {
		return this.proposalHasProperties;
	}

	public void setProposalHasProperties(List<ProposalHasProperty> proposalHasProperties) {
		this.proposalHasProperties = proposalHasProperties;
	}

	public ProposalHasProperty addProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().add(proposalHasProperty);
		proposalHasProperty.setProposal(this);

		return proposalHasProperty;
	}

	public ProposalHasProperty removeProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().remove(proposalHasProperty);
		proposalHasProperty.setProposal(null);

		return proposalHasProperty;
	}

	public List<ProfileActivityDetail> getProfileActivityDetails() {
		return this.profileActivityDetails;
	}

	public void setProfileActivityDetails(List<ProfileActivityDetail> profileActivityDetails) {
		this.profileActivityDetails = profileActivityDetails;
	}

	public ProfileActivityDetail addProfileActivityDetail(ProfileActivityDetail profileActivityDetail) {
		getProfileActivityDetails().add(profileActivityDetail);
		profileActivityDetail.setProposal(this);

		return profileActivityDetail;
	}

	public ProfileActivityDetail removeProfileActivityDetail(ProfileActivityDetail profileActivityDetail) {
		getProfileActivityDetails().remove(profileActivityDetail);
		profileActivityDetail.setProposal(null);

		return profileActivityDetail;
	}

}