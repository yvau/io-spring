package io.spring.validator;

import io.spring.constant.Variables;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 5/9/2017
 */
@Service
public class DateValidator {

    /**
     * (1) get the pattern of the date from <code>{@link Variables}</code>
     * (2) set the value of the return to false
     * (3) parse the value entered to the date_format got from (1)
     * (4) if there is an error in the parsing we return false
     * (5) return true if everything is fine
     * @param dateToValidate
     * @return true or false depending on the result
     */
    public boolean isThisDateValid(String dateToValidate) {

        SimpleDateFormat sdf = new SimpleDateFormat(Variables.DATE_FORMAT); // (1)
        sdf.setLenient(Boolean.FALSE); // (2)

        try {

            sdf.parse(dateToValidate); // (3)

        } catch (ParseException e) {

            e.printStackTrace(); // (4)
            return false;
        }

        return true; // (5)
    }

    /**
     *
     * @param dateValue
     * @return
     */
    public boolean isDateInFuture(String dateValue) {

        SimpleDateFormat sdf = new SimpleDateFormat(Variables.DATE_FORMAT);
        sdf.setLenient(Boolean.FALSE);

        try {
            if (sdf.parse(dateValue).after(new Date())) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            return false;
        }


    }
}
