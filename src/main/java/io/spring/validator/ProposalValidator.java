package io.spring.validator;

import com.google.common.base.Strings;
import io.spring.constant.Variables;
import io.spring.object.ProposalObject;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Specifies class ProposalValidator used to handle methods related to
 * <code>Proposal validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Component
public class ProposalValidator implements Validator {

    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return ProposalObject.class.isAssignableFrom(c);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ProposalObject proposalObject = (ProposalObject) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location", "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "status", "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "urgent", "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ageOfProperty", "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "priceMinimum", "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "priceMaximum", "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "size", "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "typeOfProperty", "is.required");

        if(!Strings.isNullOrEmpty(proposalObject.getTypeOfProposal())){
            if(proposalObject.getTypeOfProposal().equals(Variables.FOR_RENT)) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "isFurnished", "is.required");
            }

            if(proposalObject.getTypeOfProposal().equals(Variables.FOR_SALE)) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bedrooms", "is.required");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bathrooms", "is.required");
            }
        }
    }
}
