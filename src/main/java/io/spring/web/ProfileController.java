package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/2/2017
 */
@Controller
public class ProfileController {

    @GetMapping("/notifications/")
    public String notifications() {

        return "index";
    }

    @GetMapping("/analytics/")
    public String analytics() {

        return "index";
    }

    @GetMapping("/feeds/")
    public String feeds() {

        return "index";
    }

    @GetMapping("/bookmarks/")
    public String bookmarks() {

        return "index";
    }
}
