package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/2/2017
 */
@Controller
public class LessorController {

    @GetMapping("/profile/lease/new")
    public String create() {

        return "index";
    }

    @GetMapping("/profile/lease/edit/{id}")
    public String edit(@PathVariable Long id) {

        return "index";
    }

    @GetMapping("/lease/")
    public String overview() {

        return "index";
    }
}
