package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/2/2017
 */
@Controller
public class ProposalController {

    @GetMapping("/profile/rent/new")
    public String rentNew() {

        return "index";
    }

    @GetMapping("/profile/rent/edit/{id}")
    public String rentEdit(@PathVariable Long id) {

        return "index";
    }

    @GetMapping("/rent/")
    public String rentOverview() {

        return "index";
    }

    @GetMapping("/rent/list/")
    public String rentList() {

        return "index";
    }

    @GetMapping("/rent/{id}/")
    public String rentShow(@PathVariable Long id) {

        return "index";
    }

    @GetMapping("/profile/buy/new")
    public String buyNew() {

        return "index";
    }

    @GetMapping("/profile/buy/edit/{id}")
    public String buyEdit(@PathVariable Long id) {

        return "index";
    }

    @GetMapping("/buy/")
    public String buyOverview() {

        return "index";
    }

    @GetMapping("/buy/list/")
    public String buyList() {

        return "index";
    }

    @GetMapping("/buy/{id}/")
    public String buyShow(@PathVariable Long id) {

        return "index";
    }
}
