package io.spring.web.rest;

import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.stripe.exception.StripeException;
import io.spring.constant.Variables;
import io.spring.object.ChargeRequest;
import io.spring.service.PaypalService;
import io.spring.service.StripeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
@RestController
@RequestMapping("auth/payment/")
public class PaymentResource {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    PaypalService paypalService;

    @Autowired
    StripeService stripeService;

    @PostMapping("paypal")
    public ResponseEntity<?> payPalPayment(HttpServletRequest req) {
        Map<String, String> map = new HashMap<>();
        try {
            Payment payment = paypalService.createPayment(1.99,"fewfwe", "dqwdw", req);
            for(Links links : payment.getLinks())
                if (links.getRel().equals(Variables.APPROVAL_URL))
                    map.put(Variables.PAYMENT_ID, payment.getId());
        } catch (PayPalRESTException e){
            log.error(e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @GetMapping("cancel")
    public String cancelPay() {
        return "index";
    }

    @PostMapping("pay")
    public String successPay(@RequestParam("paymentID") String paymentId, @RequestParam("payerID") String payerId) {
        try {
            Payment payment = paypalService.executePayment(paymentId, payerId);
            if (payment.getState().equals("approved"))
                return "success";

        } catch (PayPalRESTException e) {
            log.error(e.getMessage());
        }
        return "redirect:/";
    }

    @PostMapping("charge")
    public String chargeCard(@RequestBody ChargeRequest chargeRequest) throws StripeException {

        stripeService.charge(chargeRequest);
        return "success";
    }
}
