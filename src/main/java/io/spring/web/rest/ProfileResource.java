package io.spring.web.rest;

import io.spring.model.Profile;
import io.spring.model.ProfileInformation;
import io.spring.object.ContactObject;
import io.spring.object.CredentialPassword;
import io.spring.object.Message;
import io.spring.object.ValidationResponse;
import io.spring.pageWrapper.PageWrapper;
import io.spring.security.JwtTokenUtil;
import io.spring.security.service.JwtAuthenticationResponse;
import io.spring.service.AppService;
import io.spring.service.ProfileInformationService;
import io.spring.service.ProfileService;
import io.spring.validator.ChangeBasicsValidator;
import io.spring.validator.ChangeContactsValidator;
import io.spring.validator.ChangeRolesValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Specifies controller used to handle methods related to
 * <code>ProfileResource</code>
 * (show, list...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@RestController
@RequestMapping("api/")
public class ProfileResource {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private AppService appService;

    @Autowired
    private ChangeBasicsValidator changeBasicsValidator;

    @Autowired
    private ChangeRolesValidator changeRolesValidator;

    @Autowired
    private ChangeContactsValidator changeContactsValidator;

    @Autowired
    private ProfileInformationService profileInformationService;

    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @return
     */
    @PutMapping("profile/edit/information")
    public ResponseEntity<?> updateInformation(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        // pass data to validate form
        changeBasicsValidator.validate(credentialPassword, bindingResult);
        //show error if data are not valid
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        // JwtUser jwt = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        //save data
        ProfileInformation profileInformation = profileInformationService.getById(Long.valueOf(12)).get();
        // profileInformation.setId(Long.valueOf(12));
        profileInformation.setFirstName(credentialPassword.getFirstName());
        profileInformation.setLastName(credentialPassword.getLastName());
        profileInformation.setBirthDate(credentialPassword.getBirthDate());
        profileInformation.setGender(credentialPassword.getGender());
        profileInformationService.save(profileInformation);

        //return response
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(profileInformation);
    }

    /**
     *
     * @param credentialPassword
     * @return
     */
    @PutMapping("profile/edit/role")
    public ResponseEntity<?> updateRole(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult, Device device) {
        ValidationResponse res = new ValidationResponse();
        changeRolesValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        Profile profile = profileService.getByCredential("jhyuykhjgh@ihgh.com").get();
        profile.setRole(credentialPassword.getRole());
        profileService.save(profile);

        final UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
        final String token = jwtTokenUtil.generateToken(userDetails, device);

        // Return the token
        return ResponseEntity.ok(new Message("Well done !", "Your roles have been successfully changed", new JwtAuthenticationResponse(token)));
    }

    /**
     *
     * @param contactObject
     * @param bindingResult
     * @return
     */
    @PutMapping("profile/edit/contacts")
    public ResponseEntity<?> updateContacts(@RequestBody ContactObject contactObject, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        changeContactsValidator.validate(contactObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        ProfileInformation profileInformation =  profileInformationService.getById(Long.valueOf(12)).get();
        // profileInformation.setId(Long.valueOf(12));
        profileInformation.setBestWayToReachYou(contactObject.getBestWayToReachYou());
        profileInformation.setOfficePhone(contactObject.getOfficePhone());
        profileInformation.setHomePhone(contactObject.getHomePhone());
        profileInformation.setEmailContact(contactObject.getEmailContact());

        profileInformationService.save(profileInformation);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Message("Well done !","Your informations has been changed", null));
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Message greeting() throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Message("Hello, !", "fd", null);
    }

    /**
     *
     * @param page
     * @param sorting
     * @param profile
     * @return
     */
    @GetMapping("profiles")
    public ResponseEntity<?> list(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "sorting", required = false) String sorting, Profile profile) {
        // Parse request parameters
        Pageable pageable = appService.pageable(page, null,sorting);

        PageWrapper<Profile> list = new PageWrapper<>(profileService.getAll(profile, pageable), "/profiles" );

        return ResponseEntity
                .ok(list);
    }

    @GetMapping("notifications")
    public ResponseEntity<?> notifications(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "sorting", required = false) String sorting, Profile profile) {
        // Parse request parameters
        Pageable pageable = appService.pageable(page, null, sorting);

        PageWrapper<Profile> list = new PageWrapper<>(profileService.getAll(profile, pageable), "/profiles" );

        return ResponseEntity
                .ok(list);
    }




    /**
     *
     * @return
     * @throws ClassCastException
     */
    @GetMapping("profile/get")
    public String getAuth(HttpServletRequest request) {

        return "fwfew";

        // JwtUser jwt = (JwtUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // return ResponseEntity.ok(appService.getAuth(request));

    }
}
