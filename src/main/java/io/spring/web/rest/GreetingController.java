package io.spring.web.rest;

import io.spring.object.Message;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/16/2017
 */

@Controller
public class GreetingController {

    @SendTo("/topic/greetings")
    public Message greeting() throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Message("Hello, ", "message",null );
    }
}
