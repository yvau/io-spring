package io.spring.web.rest;

import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.mail.EmailSender;
import io.spring.mail.Invite;
import io.spring.mail.MustacheCompiler;
import io.spring.model.Profile;
import io.spring.model.ProfileInformation;
import io.spring.object.CredentialPassword;
import io.spring.object.Message;
import io.spring.object.ValidationResponse;
import io.spring.security.JwtTokenUtil;
import io.spring.security.service.JwtAuthenticationResponse;
import io.spring.service.AppService;
import io.spring.service.ProfileInformationService;
import io.spring.service.ProfileService;
import io.spring.validator.RecoverValidator;
import io.spring.validator.RegisterValidator;
import io.spring.validator.ResetPasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * Specifies controller used to handle methods related to
 * <code>AuthResource</code>
 * (recover, create...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@RestController
@RequestMapping("auth/")
public class AuthResource {

    @Autowired
    private AppService appService;

    @Autowired
    private RecoverValidator recoverValidator;

    @Autowired
    private RegisterValidator registerValidator;

    @Autowired
    private ResetPasswordValidator resetPasswordValidator;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ProfileInformationService profileInformationService;

    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @return
     */
    @PostMapping("recover")
    public ResponseEntity<?> recover(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        recoverValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        Profile profile = profileService.getByCredential(credentialPassword.getCredential()).get();
        profile.setToken(Variables.THE_ID + ThreadLocalRandom.current().nextInt(1, 10));
        profileService.save(profile);

        // send a mail to the profile
        // String body = MustacheCompiler.generate(new Invite(), "invite.mustache");
        // EmailSender.send("myemail@here.com", "Welcome to our system!", body);

        // send success message to the user
        Message message = new Message("Well done !","An activation link has been sent to " + profile.getCredential() + " please click on the link to recover your password", null);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/recover")
                .buildAndExpand("")
                .toUri();

        return ResponseEntity
                .created(location)
                .body(message);
    }

    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @param request
     * @return
     * @throws MessagingException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @PostMapping("register")
    public ResponseEntity<?> create(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        registerValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }
        // send a mail to the profile
        /*String body = MustacheCompiler.generate(new Invite(), "invite.mustache");
        EmailSender.send("myemail@here.com", "Welcome to our system!", body);*/

        // set Values to profile Information
        ProfileInformation profileInformation = new ProfileInformation();
        profileInformation.setId(appService.getId(Resources.PROFILE_INFORMATION));
        profileInformation.setFirstName(credentialPassword.getFirstName());
        profileInformation.setLastName(credentialPassword.getLastName());
        profileInformationService.save(profileInformation);

        // set values to profile
        Profile profile = new Profile();
        profile.setId(appService.getId(Resources.PROFILE));
        profile.setPassword(Variables.PASSWORD_ENCODER.encode(credentialPassword.getPassword()));
        profile.setIpAddress(request.getRemoteAddr());
        profile.setToken(Variables.THE_ID + ThreadLocalRandom.current().nextInt(1, 10));
        profile.setDateOfCreationToken(appService.getDateTime(Variables.dateTime));
        profile.setDateOfCreation(appService.getDateTime(Variables.dateTime));
        profile.setCredential(credentialPassword.getCredential());
        profile.setCredentialsNonExpired(Boolean.TRUE);
        profile.setEnabled(Boolean.FALSE);
        profile.setAccountNonExpired(Boolean.TRUE);
        profile.setAccountNonLocked(Boolean.TRUE);
        profile.setRole(Variables.ROLE_LIMITED);
        profile.setProfileInformation(profileInformation);
        profileService.save(profile);

        // send success message to the user
        Message message = new Message("Well done !","An activation link has been sent to " + profile.getCredential() + " please click on the link to activate your account", null);


        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/register")
                .buildAndExpand(profile.getId())
                .toUri();

        return ResponseEntity
                .created(location)
                .body(message);
    }

    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @return
     */
    @PostMapping("password/reset")
    public ResponseEntity<?> reset(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        resetPasswordValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        Profile profile = profileService.getByToken(credentialPassword.getToken()).get();
        profile.setPassword(Variables.PASSWORD_ENCODER.encode(credentialPassword.getPassword()));
        profile.setToken(null);
        profileService.save(profile);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("reset/pwd")
                .buildAndExpand(profile.getId())
                .toUri();

        return ResponseEntity
                .created(location)
                .body("jkj");
    }

    /**
     *
     * @param token
     * @return
     */
    @GetMapping("activate")
    public ResponseEntity<?> activate(@RequestParam(value = "token") String token, Device device){
        Optional<Profile> profile = profileService.getByToken(token);
        if(!profile.isPresent())
            return ResponseEntity.ok(new Message("Error !","the token provided is not valid or has expired", null));

        // Reload password post-security so we can generate token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(profile.get().getCredential());
        final String tokenAuth = jwtTokenUtil.generateToken(userDetails, device);

        profile.get().setEnabled(Boolean.TRUE);
        profile.get().setToken(null);
        profileService.save(profile.get());

        // Return the token
        return ResponseEntity.ok(new JwtAuthenticationResponse(tokenAuth));
    }

    /**
     *
     * @param token
     * @return
     */
    @GetMapping("recover")
    public ResponseEntity<?> recover(@RequestParam(value = "token") String token){
        Optional<Profile> profile = profileService.getByToken(token);
        if(!profile.isPresent())
            return ResponseEntity.ok(new Message("Error !","the token provided is not valid or has expired", null));

        // Return the token
        return ResponseEntity.ok(new Message("Success",token, null));
    }

}
