package io.spring.web.rest;

import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.model.*;
import io.spring.object.Message;
import io.spring.object.PropertyObject;
import io.spring.object.ValidationResponse;
import io.spring.pageWrapper.PageWrapper;
import io.spring.service.*;
import io.spring.validator.PropertyValidator;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Specifies controller used to handle methods related to
 * <code>PropertyResource</code>
 * (show, list...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@RestController
@RequestMapping("api/")
public class PropertyResource {

    private String fileUploadDirectory = "C:\\wamp64\\www\\demo\\";

    @Autowired
    private PropertyValidator propertyValidator;

    @Autowired
    private LocationService locationService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private PropertyPhotoService propertyPhotoService;

    @Autowired
    private CityService cityService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private AppService appService;

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/property/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        // if(!property.isPresent())throw new NoHandlerFoundException(null, null, null);
        Optional<Property> property = Optional.ofNullable(propertyService.getById(id));

        return ResponseEntity.ok(property.get());
    }

    /**
     *
     * @param page
     * @param sorting
     * @param property
     * @return
     */
    @GetMapping("property/list")
    public ResponseEntity<?> list(@RequestParam(value = "page", required = false) Integer page,
                                        @RequestParam(value = "sorting", required = false) String sorting, Property property) {
        // Parse request parameters
        Pageable pageable = appService.pageable(page, null,sorting);

        PageWrapper<Property> list = new PageWrapper<>(propertyService.findAll(property, pageable), "/api/property/list" );

        return ResponseEntity
                .ok(list);
    }

    /**
     *
     * @param propertyObject
     * @param bindingResult
     * @return
     */
    @PostMapping("profile/property/new")
    @PreAuthorize("hasAnyRole('ROLE_SELLER','ROLE_LESSOR')")
    public ResponseEntity<?> create(PropertyObject propertyObject, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        propertyValidator.validate(propertyObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }
        Profile profile = profileService.getByCredential("jhyuykhjgh@ihgh.com").get();
        City city = cityService.getById(propertyObject.getLocation()).get();

        Location location = new Location();
        location.setId(UUID.randomUUID().toString()+appService.getId(Resources.LOCATION));
        location.setAddress(propertyObject.getAddress());
        location.setCity(city);
        location.setProvince(city.getProvince());
        location.setCountry(city.getProvince().getCountry());
        location.setPostalCode(propertyObject.getPostalCode());


        Property property = new Property();
        property.setId(appService.getId(Resources.PROPERTY));
        property.setPrice(propertyObject.getPrice());
        property.setBedrooms(propertyObject.getBedrooms());
        property.setBathrooms(propertyObject.getBathrooms());
        property.setType(propertyObject.getType());
        property.setDateOfCreation(Timestamp.from(Variables.timestamp));
        property.setEnabled(Boolean.FALSE);
        property.setStatus(propertyObject.getStatus());
        property.setSize(propertyObject.getSize());
        property.setStatus(propertyObject.getStatus());
        property.setSaleType(propertyObject.getSaleType());
        property.setDescription(propertyObject.getDescription());
        property.setCharacteristics(propertyObject.getCharacteristics());
        property.setLocation(location);
        property.setProfile(profile);
        propertyService.save(property);


        if (propertyObject.getPropertyPhotos() != null) {
            propertyObject.getPropertyPhotos().stream().forEach((MultipartFile uploadedFile) -> {
                String newFilenameBase = UUID.randomUUID().toString();
                String originalFileExtension = uploadedFile.getOriginalFilename().substring(uploadedFile.getOriginalFilename().lastIndexOf("."));
                String newFilename = newFilenameBase + originalFileExtension;

                try {
                    File file = new File(fileUploadDirectory + newFilename);
                    uploadedFile.transferTo(file);
                    BufferedImage thumbnail = Scalr.resize(ImageIO.read(file), 290);
                    String thumbnailFilename = newFilenameBase + "-thumbnail.png";
                    File thumbnailFile = new File(fileUploadDirectory + thumbnailFilename);
                    ImageIO.write(thumbnail, "png", thumbnailFile);

                    PropertyPhoto propertyPhoto = new PropertyPhoto();
                    propertyPhoto.setId(appService.getId(Resources.PROPERTY_PHOTO));
                    propertyPhoto.setProperty(property);
                    propertyPhoto.setContentType(uploadedFile.getContentType());
                    propertyPhoto.setName(newFilename);
                    propertyPhoto.setSize(BigDecimal.valueOf(uploadedFile.getSize()));
                    propertyPhoto.setThumbnailName(thumbnailFilename);
                    propertyPhoto.setThumbnailSize(BigDecimal.valueOf(thumbnailFile.length()));
                    propertyPhoto.setCreatedDate(Timestamp.from(Variables.timestamp));
                    propertyPhotoService.save(propertyPhoto);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }


        URI locate = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(property.getType())
                .toUri();

        return ResponseEntity
                .created(locate)
                .body(new Message("Well done !", "Your property has been successfully saved", null));

    }

    /**
     *
     * @param propertyObject
     * @param bindingResult
     * @return
     */
    @PostMapping("profile/property/update")
    public ResponseEntity<?> update(PropertyObject propertyObject, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        propertyValidator.validate(propertyObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }
        Profile profile = profileService.getByCredential("jhyuykhjgh@ihgh.com").get();
        City city = cityService.getById(propertyObject.getLocation()).get();
        Property property = propertyService.getById(propertyObject.getId());

        Location location = locationService.getById(propertyObject.getLocation()).get();
        location.setId(propertyObject.getLocation());
        location.setAddress(propertyObject.getAddress());
        location.setCity(city);
        location.setProvince(city.getProvince());
        location.setCountry(city.getProvince().getCountry());
        location.setPostalCode(propertyObject.getPostalCode());

        property.setId(propertyObject.getId());
        property.setPrice(propertyObject.getPrice());
        property.setBedrooms(propertyObject.getBedrooms());
        property.setBathrooms(propertyObject.getBathrooms());
        property.setType(propertyObject.getType());
        property.setDateOfCreation(Timestamp.from(Variables.timestamp));
        property.setEnabled(Boolean.FALSE);
        property.setStatus(propertyObject.getStatus());
        property.setSize(propertyObject.getSize());
        property.setStatus(propertyObject.getStatus());
        property.setSaleType(propertyObject.getSaleType());
        property.setDescription(propertyObject.getDescription());
        property.setCharacteristics(propertyObject.getCharacteristics());
        property.setLocation(location);
        property.setProfile(profile);
        propertyService.save(property);


        if (propertyObject.getPropertyPhotos() != null) {
            propertyObject.getPropertyPhotos().stream().forEach((MultipartFile uploadedFile) -> {
                String newFilenameBase = UUID.randomUUID().toString();
                String originalFileExtension = uploadedFile.getOriginalFilename().substring(uploadedFile.getOriginalFilename().lastIndexOf("."));
                String newFilename = newFilenameBase + originalFileExtension;

                try {
                    File file = new File(fileUploadDirectory + newFilename);
                    uploadedFile.transferTo(file);
                    BufferedImage thumbnail = Scalr.resize(ImageIO.read(file), 290);
                    String thumbnailFilename = newFilenameBase + "-thumbnail.png";
                    File thumbnailFile = new File(fileUploadDirectory + thumbnailFilename);
                    ImageIO.write(thumbnail, "png", thumbnailFile);

                    PropertyPhoto propertyPhoto = new PropertyPhoto();
                    propertyPhoto.setId(appService.getId(Resources.PROPERTY_PHOTO));
                    propertyPhoto.setProperty(property);
                    propertyPhoto.setContentType(uploadedFile.getContentType());
                    propertyPhoto.setName(newFilename);
                    propertyPhoto.setSize(BigDecimal.valueOf(uploadedFile.getSize()));
                    propertyPhoto.setThumbnailName(thumbnailFilename);
                    propertyPhoto.setThumbnailSize(BigDecimal.valueOf(thumbnailFile.length()));
                    propertyPhoto.setCreatedDate(Timestamp.from(Variables.timestamp));
                    propertyPhotoService.save(propertyPhoto);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }


        URI locate = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(property.getType())
                .toUri();

        return ResponseEntity
                .created(locate)
                .body(new Message("Well done !", "Your property has been successfully saved", null));

    }

    /**
     *
     * @param propertyObject
     * @param bindingResult
     * @return
     */
   /* @PutMapping("profile/property/update")
    //  @PreAuthorize ("#property?.profile?.credential == authentication.name")
    //	@PreAuthorize("@employeeRepository.findOne(#id)?.manager?.name == authentication?.name")
    public ResponseEntity<?> update(PropertyObject propertyObject, BindingResult bindingResult) {
        System.out.println(propertyObject.getSaleType());
        ValidationResponse res = new ValidationResponse();
        propertyValidator.validate(propertyObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }
        City city = cityService.getById(propertyObject.getLocation()).get();

        Property property = propertyService.getById(propertyObject.getId());

        Location location = locationService.getById(property.getLocation().getId()).get();
        location.setId(property.getLocation().getId());
        location.setAddress(propertyObject.getAddress());
        location.setCity(city);
        location.setProvince(city.getProvince());
        location.setCountry(city.getProvince().getCountry());
        location.setPostalCode(propertyObject.getPostalCode());


        property.setId(propertyObject.getId());
        property.setPrice(propertyObject.getPrice());
        property.setBedrooms(propertyObject.getBedrooms());
        property.setBathrooms(propertyObject.getBathrooms());
        property.setType(propertyObject.getType());
        property.setDateOfCreation(Timestamp.from(Variables.timestamp));
        property.setSize(propertyObject.getSize());
        property.setStatus(propertyObject.getStatus());
        property.setSaleType(propertyObject.getSaleType());
        property.setDescription(propertyObject.getDescription());
        property.setCharacteristics(propertyObject.getCharacteristics());
        property.setLocation(location);
        // property.setProfile(authenticationService.getUserAuthenticated());
        propertyService.save(property);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(property);
    }*/

    /********************************** HELPER METHOD **********************************/
    /*private ResponseEntity<Property> assertExist(Long id) {
        return Optional.ofNullable(propertyService.getById(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException()
                        .setId(Long.toString(id))
                        .setResourceName(Resources.PROPERTY));
    }*/
}
