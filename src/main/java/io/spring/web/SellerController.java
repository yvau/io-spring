package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/2/2017
 */
@Controller
public class SellerController {

    @GetMapping("/profile/sell/new")
    public String create() {

        return "index";
    }

    @GetMapping("/profile/sell/edit/{id}")
    public String edit() {

        return "index";
    }

    @GetMapping("/sell/")
    public String overview() {

        return "index";
    }
}
