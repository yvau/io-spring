package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/16/2017
 */

@Controller
public class AuthController {

    /**
     *
     * @return
     */
    @GetMapping("/register/")
    public String register(Model model, HttpServletRequest request) {
        
        return "index";
    }

    @GetMapping("/login/")
    public String login() {

        return "index";
    }

    @GetMapping("/recover/")
    public String recover() {

        return "index";
    }

    @GetMapping("/activate/")
    public String activate() {

        return "index";
    }


}
