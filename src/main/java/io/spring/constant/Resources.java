package io.spring.constant;

/**
 * Specifies class used to gather multiples variables constant related to
 * <code>Entities</code>
 * (PROFILE, LOCATION, PROPOSAL...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class Resources {

    public static final String PROFILE = "profile";
    public static final String PROFILE_INFORMATION = "profile_information";
    public static final String LOCATION = "location";
    public static final String PROPERTY = "property";
    public static final String PROPOSAL = "proposal";
    public static final String SEARCH_PROPOSAL_DETAIL = "search_proposal_detail";
    public static final String SEARCH_PROPOSAL = "search_proposal";
    public static final String PROPERTY_PHOTO = "property_photo";

    /**
     * Prevent instantiation
     */
    private Resources() {
    }

}
