package io.spring.constant;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

/**
 * Specifies class used to gather multiples variables constant
 * (THE_ID, UTF8, PASSWORD_PATTERN...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class Variables {

    public static final Instant timestamp = Instant.now();
    public static final String FOR_SALE = "for_sale"; // For sale
    public static final String FOR_RENT = "for_rent"; // For rent
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String APPROVAL_URL = "approval_url";
    public static final String PAYMENT_ID = "paymentID";

    //Minimum 6 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:
    public static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    public static final Date dateTime = new Date();
    public static final String ROLE_LIMITED = "ROLE_LIMITED";
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    public static final String THE_ID = UUID.randomUUID().toString();
    public static final String UTF8 = "UTF-8";
    /**
     * Prevent instantiation
     */
    private Variables() {
    }
}
