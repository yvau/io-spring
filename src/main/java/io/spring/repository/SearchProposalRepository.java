package io.spring.repository;

import io.spring.model.SearchProposal;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Specifies class SearchProposalRepository used to handle methods related to
 * <code>{@link JpaRepository}</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public interface SearchProposalRepository extends JpaRepository<SearchProposal, Long> {
}
