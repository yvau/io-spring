package io.spring.repository;

import io.spring.model.SearchProposalDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Specifies class SearchProposalRepository used to handle methods related to
 * <code>{@link JpaRepository}</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public interface SearchProposalDetailRepository extends JpaRepository<SearchProposalDetail, Long> {
}
