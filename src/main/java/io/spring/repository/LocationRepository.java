package io.spring.repository;

import io.spring.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Specifies class LocationRepository used to handle methods related to
 * <code>{@link JpaRepository}</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public interface LocationRepository extends JpaRepository<Location, String> {

}
