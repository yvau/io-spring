package io.spring.object;

import java.util.List;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 9/29/2017
 */
public class SearchProposalObject {

    private String ageOfProperty;

    private Boolean isFirstBuyer;

    private Boolean isUrgent;

    //bi-directional many-to-one association to LocationHasFileOfNeedSellerLessorPropertyDetail
    private List<SearchProposalDetailsObject> searchProposalDetails;

    public String getAgeOfProperty() {
        return ageOfProperty;
    }

    public void setAgeOfProperty(String ageOfProperty) {
        this.ageOfProperty = ageOfProperty;
    }

    public Boolean getFirstBuyer() {
        return isFirstBuyer;
    }

    public void setFirstBuyer(Boolean firstBuyer) {
        isFirstBuyer = firstBuyer;
    }

    public Boolean getUrgent() {
        return isUrgent;
    }

    public void setUrgent(Boolean urgent) {
        isUrgent = urgent;
    }

    public List<SearchProposalDetailsObject> getSearchProposalDetails() {
        return searchProposalDetails;
    }

    public void setSearchProposalDetails(List<SearchProposalDetailsObject> searchProposalDetails) {
        this.searchProposalDetails = searchProposalDetails;
    }
}
