package io.spring.object;

import org.hibernate.annotations.Type;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Lob;
import java.math.BigDecimal;
import java.util.List;

/**
 * Specifies class Property Object used to handle methods related to
 * <code>Property Object</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/20/2017
 */
public class PropertyObject {

    private Long id;

    private String location;

    private String address;

    private String postalCode;

    private String saleType;

    private String type;

    private String status;

    private Integer bathrooms;

    private Integer bedrooms;

    private BigDecimal price;

    private BigDecimal size;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String characteristics;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    private List<MultipartFile> propertyPhotos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(Integer bathrooms) {
        this.bathrooms = bathrooms;
    }

    public Integer getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    public List<MultipartFile> getPropertyPhotos() {
        return propertyPhotos;
    }

    public void setPropertyPhotos(List<MultipartFile> propertyPhotos) {
        this.propertyPhotos = propertyPhotos;
    }
}
