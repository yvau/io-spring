package io.spring.object;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/11/2017
 */
public class ContactObject {

    private String bestWayToReachYou;

    private String homePhone;

    private String officePhone;

    private String emailContact;

    public String getBestWayToReachYou() {
        return bestWayToReachYou;
    }

    public void setBestWayToReachYou(String bestWayToReachYou) {
        this.bestWayToReachYou = bestWayToReachYou;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getEmailContact() {
        return emailContact;
    }

    public void setEmailContact(String emailContact) {
        this.emailContact = emailContact;
    }
}
