package io.spring.object;

import java.util.List;

/**
 * Specifies class Validation Response used to handle methods related to
 * <code>Validation Response</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/21/2017
 */
public class ValidationResponse {
    private String action;
    private List<Message> messageList;

    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

    public List<Message> getMessageList() {
        return this.messageList;
    }
    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }
}
