package io.spring.object;

import java.math.BigDecimal;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
public class ChargeRequest {

    private String description;
    private BigDecimal amount; // cents
    private String user;
    private String token;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}