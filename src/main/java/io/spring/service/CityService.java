package io.spring.service;

import io.spring.model.City;

import java.util.List;
import java.util.Optional;

/**
 * Specifies class CityService used to handle methods related to
 * <code>City Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
public interface CityService {

    List<City> findByName(String name);

    Optional<City> getById(final String id);
}
