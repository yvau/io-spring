package io.spring.service;

import io.spring.constant.Numeric;
import io.spring.model.AutoIncrement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

/**
 * Specifies class AppService used to handle methods related to
 * <code>App</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
@Service
public class AppServiceImpl implements AppService {

    private final AutoIncrementService autoIncrementService;

    @Autowired
    public AppServiceImpl(final AutoIncrementService autoIncrementService) {
        this.autoIncrementService = autoIncrementService;
    }

    /**
     * @param dateTime
     * @return
     */
    public Timestamp getDateTime(Date dateTime){
        return new Timestamp(dateTime.getTime());
    }

    /*public Profile getAuth(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        return user.getProfile();
    }*/
    /**
     *
     * @param value
     * @return
     */
    public boolean isNumeric(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     *
     * @param resource
     * @return
     */
    public Long getId(String resource) {
        //get the number of the last id entered from the resources eg.proposal, property
        Optional<AutoIncrement> autoIncrementFor  = autoIncrementService.getById(resource);
        Long id = Long.valueOf(Numeric.NULL_VALUE);
        /**
        * if the resource is not found we save the new resource
        */
        if(!autoIncrementFor.isPresent()) {
            AutoIncrement autoIncrement = new AutoIncrement();
            autoIncrement.setId(resource);
            autoIncrement.setIncrementNumber(++id);

            autoIncrementService.save(autoIncrement);
        } else {
            /**
             * (1) get the id of the last resource from my entity "AutoIncrement" and increment it + 1
             * (2) set new value of number increment and save it
             */
            id = autoIncrementFor.get().getIncrementNumber(); // (1)
            autoIncrementFor.get().setIncrementNumber(++id); // (2)
            autoIncrementService.save(autoIncrementFor.get()); // (2)
        }

        return id;
    }

    /**
     *
     * @param sorting
     * @return
     */
    private Sort orderBy(String sorting) {
        sorting = sorting == null ? "id-desc" : sorting;
        String[] sortingArray = sorting.split("-");
        Direction direction = sortingArray[1].toUpperCase().equals("ASC") ? Direction.ASC : Direction.DESC;
        return new Sort(direction, sortingArray[0]);
    }

    /**
     *
     * @param page
     * @param sorting
     * @return
     */
    public Pageable pageable(Integer page, Integer size, String sorting) {
        page = page == null ? Numeric.NULL_VALUE : (page.intValue() - 1);
        size = size == null ? Numeric.TEN_VALUE : size;
        Sort sort = orderBy(sorting);
        return new PageRequest(page, size, sort);
    }

}
