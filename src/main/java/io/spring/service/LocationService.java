package io.spring.service;

import io.spring.model.Location;

import java.util.List;
import java.util.Optional;

/**
 * Specifies class LocationService used to handle methods related to
 * <code>Location Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
public interface LocationService {

	void save(Location location);

	void delete(List<Location> locationList);

	Optional<Location> getById(final String id);
}
