package io.spring.service;

import io.spring.model.SearchProposal;
import io.spring.repository.SearchProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Specifies class SearchProposalService used to handle methods related to
 * <code>SearchProposal Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class SearchProposalServiceImpl implements SearchProposalService {

    @Autowired
    SearchProposalRepository repository;

    /**
     * save <code>{@link SearchProposal}</code>
     * @param searchProposal
     */
    @Override
    // @PreAuthorize("hasAnyRole('ROLE_SELLER','ROLE_LESSOR') and #searchProposal?.profile?.credential == authentication?.name")
    public void save(@Param("searchProposal") SearchProposal searchProposal) {
        repository.saveAndFlush(searchProposal);
        return;
    }

    /**
     *
     * @param id
     * @return optional searchProposal
     */
    @Override
    public Optional<SearchProposal> getById(final Long id) {
        return Optional.ofNullable(repository.findOne(id));
    }
}
