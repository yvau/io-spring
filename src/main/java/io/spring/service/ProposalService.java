package io.spring.service;

import io.spring.model.Proposal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Specifies class ProposalService used to handle methods related to
 * <code>Proposal Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface ProposalService {

	void save(Proposal proposal);

	Proposal getById(final Long id);

	Proposal getByIdAndTypeOfProposal(final Long id, final String typeOfProposal);

    Page<Proposal> findAll(Proposal proposal, Pageable pageable);

}
