package io.spring.service;

import io.spring.model.PropertyPhoto;

/**
 * Specifies class PropertyPhotoService used to handle methods related to
 * <code>PropertyPhoto Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface PropertyPhotoService {

    PropertyPhoto save(PropertyPhoto propertyPhoto);
}
