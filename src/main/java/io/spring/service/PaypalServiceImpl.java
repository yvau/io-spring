package io.spring.service;

import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import io.spring.common.utils.UrlUtils;
import io.spring.enumeration.EnumLabel;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
@Service
public class PaypalServiceImpl implements PaypalService {

    public static final String mode = "sandbox";
    public static final String client = "ASSljN1X2fhtOalonExcpolVsWmMWGVBI-NiTSiQpkksE02CFoenGaBlT13DRxlJxG03g5UuKTuScwpj";
    public static final String secret = "EDJ8tBVgcQthBJBA6odKasLX0kaOQuzQMCLB7kdembe-GRuPZiv48SzrIrvsrWaCXX2mKQATGPyFC28b";
    public static final String currency = "CAD";
    public static final String cancelUrl = "/cancel";
    public static final String successUrl = "/pay";

    APIContext apiContext = new APIContext(client, secret, mode);

    public Payment createPayment(
            Double total,
            String custom,
            String description,
            HttpServletRequest req) throws PayPalRESTException {
        Amount amount = new Amount();
        amount.setCurrency(currency);
        amount.setTotal(String.format("%.2f", total));

        Transaction transaction = new Transaction();
        transaction.setCustom(custom);
        transaction.setDescription(description);
        transaction.setAmount(amount);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        Payer payer = new Payer();
        payer.setPaymentMethod(EnumLabel.PaymentMethod.paypal.toString());

        Payment payment = new Payment();
        payment.setIntent(EnumLabel.PaymentIntent.sale.toString());
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl(UrlUtils.getBaseURl(req, cancelUrl));
        redirectUrls.setReturnUrl(UrlUtils.getBaseURl(req, successUrl));
        payment.setRedirectUrls(redirectUrls);

        return payment.create(apiContext);
    }

    public Payment executePayment(String paymentId, String payerId) throws PayPalRESTException{
        Payment payment = new Payment();
        payment.setId(paymentId);
        PaymentExecution paymentExecute = new PaymentExecution();
        paymentExecute.setPayerId(payerId);
        return payment.execute(apiContext, paymentExecute);
    }
}
