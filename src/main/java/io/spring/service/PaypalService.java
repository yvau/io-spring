package io.spring.service;

import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
public interface PaypalService {

    Payment createPayment(Double total, String custom, String description, HttpServletRequest req) throws PayPalRESTException;

    Payment executePayment(String paymentId, String payerId) throws PayPalRESTException;
}
