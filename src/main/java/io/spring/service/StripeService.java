package io.spring.service;

import com.stripe.exception.*;
import com.stripe.model.Charge;
import io.spring.object.ChargeRequest;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
public interface StripeService {

	Charge charge(ChargeRequest chargeRequest) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException;
}
