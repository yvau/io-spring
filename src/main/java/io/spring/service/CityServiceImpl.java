package io.spring.service;

import io.spring.model.City;
import io.spring.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Specifies class CityService used to handle methods related to
 * <code>City Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
@Service
public class CityServiceImpl implements CityService {

    private final CityRepository repository;

    @Autowired
    public CityServiceImpl(final CityRepository repository) {
        this.repository = repository;
    }

    /**
     *
     * @param name
     * @return
     */
    @Override
    public List<City> findByName(String name) {
        try {
            return repository.findFirst300ByNameAsciiContainingIgnoreCase(name);
        } catch (Exception e) {
            return null;// TODO: handle exception
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Optional<City> getById(final String id) {
        return Optional.ofNullable(repository.findOne(id));
    }

}
