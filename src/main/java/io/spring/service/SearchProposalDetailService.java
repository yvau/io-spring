package io.spring.service;

import io.spring.model.SearchProposalDetail;


/**
 * Specifies class SearchProposalService used to handle methods related to
 * <code>SearchProposal Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface SearchProposalDetailService {

	void save(SearchProposalDetail searchProposalDetail);

}
