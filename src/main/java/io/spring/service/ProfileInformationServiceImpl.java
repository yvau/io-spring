package io.spring.service;

import io.spring.model.ProfileInformation;
import io.spring.repository.ProfileInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Specifies class ProfileInformationService used to handle methods related to
 * <code>ProfileInformation Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
@Service
public class ProfileInformationServiceImpl implements ProfileInformationService {

	private final ProfileInformationRepository repository;

	@Autowired
	public ProfileInformationServiceImpl(final ProfileInformationRepository repository) {
		this.repository = repository;
	}

	/**
	 *
	 * @param profileInformation
	 */
	@Override
	public void save (ProfileInformation profileInformation) {
		repository.save(profileInformation);
		return;
	}

	@Override
	public Optional<ProfileInformation> getById(Long id) {
		return Optional.ofNullable(repository.findOne(id));
	}
}
