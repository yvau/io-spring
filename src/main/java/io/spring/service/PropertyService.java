package io.spring.service;

import io.spring.model.Property;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Specifies class PropertyService used to handle methods related to
 * <code>Property Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface PropertyService {

	void save(Property property);

	Property getById(final Long id);

	Page<Property> findAll(Property property, Pageable pageable);
}
