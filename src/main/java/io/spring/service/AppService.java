package io.spring.service;

import org.springframework.data.domain.Pageable;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Specifies class AppService used to handle methods related to
 * <code>App</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
public interface AppService {

    Timestamp getDateTime(Date Datetime);

    Pageable pageable(Integer page, Integer size, String sorting);

    Long getId(String resource);

    // Profile getAuth(HttpServletRequest request);

}

