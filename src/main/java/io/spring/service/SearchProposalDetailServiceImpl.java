package io.spring.service;

import io.spring.model.SearchProposalDetail;
import io.spring.repository.SearchProposalDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

/**
 * Specifies class SearchProposalService used to handle methods related to
 * <code>SearchProposal Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class SearchProposalDetailServiceImpl implements SearchProposalDetailService {

    @Autowired
    SearchProposalDetailRepository repository;

    /**
     * save <code>{@link SearchProposalDetail}</code>
     * @param searchProposalDetail
     */
    @Override
    public void save(@Param("searchProposalDetail") SearchProposalDetail searchProposalDetail) {
        repository.saveAndFlush(searchProposalDetail);
        return;
    }

}
