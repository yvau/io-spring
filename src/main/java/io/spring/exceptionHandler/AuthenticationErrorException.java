package io.spring.exceptionHandler;

import org.springframework.security.core.AuthenticationException;

/**
 * A exceção <code>LibraryAuthenticationErrorException</code> indica 
 * erros no processo de autenticação. Ela é genérica e cabe as exceções
 * filhas especificarem o erro que representam.
 *
 * @author Augusto dos Santos
 * @version 1.0 17 de jan de 2017
 */
public class AuthenticationErrorException extends AuthenticationException {
	
	private static final long serialVersionUID = 1L;

	public AuthenticationErrorException(String msg) {
		super(msg);
	}
}
