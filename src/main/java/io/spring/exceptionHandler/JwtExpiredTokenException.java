package io.spring.exceptionHandler;

/**
 * A exceção <code>JwtExpiredTokenException</code> indica 
 * que o JWT passado no processo de autenticação já expirou.
 *
 * @author Augusto dos Santos
 * @version 1.0 13 de jan de 2017
 */
public class JwtExpiredTokenException extends AuthenticationErrorException {

	private static final long serialVersionUID = -5959543783324224864L;
    
    public JwtExpiredTokenException(String msg) {
        super(msg);
    }

}
