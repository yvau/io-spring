package io.spring.common.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
public class UrlUtils {
    public static String getBaseURl(HttpServletRequest request, String value) {
        String scheme = request.getScheme();
        String serverName = request.getServerName();
        // int serverPort = request.getServerPort();
        String contextPath = request.getContextPath();
        StringBuffer url =  new StringBuffer();
        url.append(scheme).append("://").append(serverName);
        /*if ((serverPort != 80) && (serverPort != 443)) {
            url.append(":").append(serverPort);
        }*/
        url.append(contextPath);
        if(url.toString().endsWith("/")){
            url.append("/");
        }
        return url.toString() + "/" + value;
    }
}
